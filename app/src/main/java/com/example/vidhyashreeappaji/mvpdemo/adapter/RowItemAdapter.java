package com.example.vidhyashreeappaji.mvpdemo.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.vidhyashreeappaji.mvpdemo.R;
import com.example.vidhyashreeappaji.mvpdemo.model.RowItem;

import java.util.ArrayList;

public class RowItemAdapter extends RecyclerView.Adapter<RowItemAdapter.ViewHolder> {

    private ArrayList<RowItem> rowList;
    private Context mContext;

    public RowItemAdapter(Context context, ArrayList<RowItem> rowList) {
        this.rowList = rowList;
        mContext = context;
    }

    @NonNull
    @Override
    public RowItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RowItemAdapter.ViewHolder viewHolder, int position) {
        RowItem rowItem = rowList.get(position);

        if (isNullorEmpty(rowItem.getDescription()) && isNullorEmpty(rowItem.getTitle()) && isNullorEmpty(rowItem.getImageHref())) {
            rowList.remove(rowItem);
            notifyDataSetChanged();
        }


        //loading the data
        else {
            viewHolder.mTitle.setText(rowItem.getTitle());
            viewHolder.mDescription.setText(rowItem.getDescription());

            //image loading using Picasso
            Glide.with(mContext).load(rowList.get(position).getImageHref()).into(viewHolder.mImage);
        }
    }

    @Override
    public int getItemCount() {
        return rowList.size();
    }

    private boolean isNullorEmpty(String s) {
        if (s == null) {
            return true;
        }
        if (s.isEmpty())
            return true;

        return false;

    }

    //View holder class for individual views
    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitle;
        private TextView mDescription;
        private ImageView mImage;

        ViewHolder(View itemView) {
            super(itemView);

            mTitle = itemView.findViewById(R.id.row_title);
            mDescription = itemView.findViewById(R.id.row_description);
            mImage = itemView.findViewById(R.id.row_image);
            //row item clicklistener for onclick events
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, rowList.get(getAdapterPosition()).getTitle(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}

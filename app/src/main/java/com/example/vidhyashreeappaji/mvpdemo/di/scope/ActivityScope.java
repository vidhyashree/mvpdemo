package com.example.vidhyashreeappaji.mvpdemo.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;
// ActivityScope for components
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityScope {
}

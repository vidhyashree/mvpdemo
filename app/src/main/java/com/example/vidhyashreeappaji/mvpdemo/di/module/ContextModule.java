package com.example.vidhyashreeappaji.mvpdemo.di.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
//Provide the application context
@Module
public class ContextModule {
    private Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context provideContext() {
        return context;
    }
}

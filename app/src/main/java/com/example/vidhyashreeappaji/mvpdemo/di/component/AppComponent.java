package com.example.vidhyashreeappaji.mvpdemo.di.component;

import android.app.Application;
import android.content.Context;

import com.example.vidhyashreeappaji.mvpdemo.MyApplication;
import com.example.vidhyashreeappaji.mvpdemo.di.module.AppModule;
import com.example.vidhyashreeappaji.mvpdemo.di.module.ContextModule;

import javax.inject.Singleton;

import dagger.Component;

//This is the App level Component. The fields would be accessible throughout the application.
@Singleton
@Component(modules = {AppModule.class, ContextModule.class})
public interface AppComponent {
    void inject(MyApplication myApplication);

    Context getContext();

    Application getApplication();
}


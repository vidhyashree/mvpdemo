package com.example.vidhyashreeappaji.mvpdemo.di.module;

import android.content.Context;

import com.example.vidhyashreeappaji.mvpdemo.mvp.interactor.Interactor;
import com.example.vidhyashreeappaji.mvpdemo.mvp.interactor.InteractorImpl;

import dagger.Module;
import dagger.Provides;

//This provides an instance of Model(InteractorImpl) class for us.
@Module
public class InteractorModule {

    @Provides
    Interactor provideInteractor(Context context) {
        return new InteractorImpl(context);
    }
}

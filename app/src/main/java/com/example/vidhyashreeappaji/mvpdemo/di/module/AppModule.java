package com.example.vidhyashreeappaji.mvpdemo.di.module;

import android.app.Application;

import com.example.vidhyashreeappaji.mvpdemo.MyApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

//Provides application instance across the project
@Module
public class AppModule {

    private MyApplication myApplication;

    public AppModule(MyApplication myApplication) {
        this.myApplication = myApplication;
    }

    @Provides
    @Singleton
    public Application provideApplication() {
        return myApplication;
    }
}

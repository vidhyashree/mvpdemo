package com.example.vidhyashreeappaji.mvpdemo.mvp.interactor;

import com.example.vidhyashreeappaji.mvpdemo.model.ListItem;

//Model has an interface onFinished which triggers after the handler interval succeeds.
public interface Interactor {
    //fetch new data from the server
    void fetchNewDataFromServer(OnFinishedListener onFinishedListener);

    interface OnFinishedListener {
        //on request finished
        void onFinished(ListItem item);
        //on request failure
        void onFailure(Throwable t);

    }


}

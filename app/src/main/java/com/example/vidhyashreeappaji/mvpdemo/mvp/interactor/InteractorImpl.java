package com.example.vidhyashreeappaji.mvpdemo.mvp.interactor;

import android.content.Context;
import android.support.annotation.NonNull;

import com.example.vidhyashreeappaji.mvpdemo.model.ListItem;
import com.example.vidhyashreeappaji.mvpdemo.retrofit.APIInterface;
import com.example.vidhyashreeappaji.mvpdemo.retrofit.RetroFitAPIClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InteractorImpl implements Interactor {

    private Context context;

    public InteractorImpl(Context context) {
        this.context=context;
    }

    //fetch new data from the server
    @Override
    public void fetchNewDataFromServer(final OnFinishedListener onFinishedListener) {
        //Retrofit call to fetch data
        Call<ListItem> responseObj = RetroFitAPIClient.getRetrofitInstance(context).create(APIInterface.class).getAllItems();
        responseObj.enqueue(new Callback<ListItem>() {
            @Override
            public void onResponse(@NonNull Call<ListItem> call, @NonNull Response<ListItem> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        onFinishedListener.onFinished(response.body());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ListItem> call, @NonNull Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }

}

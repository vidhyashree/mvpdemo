package com.example.vidhyashreeappaji.mvpdemo.mvp.presenter;

//The Presenter controls the swipe to refresh,fetch data and onDestroy() lifecycle.
public interface MainPresenter {

    void onRefresh();

    void fetchDatafromServer();

    void onDestroy();
}

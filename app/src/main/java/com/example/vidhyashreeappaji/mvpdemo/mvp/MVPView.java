package com.example.vidhyashreeappaji.mvpdemo.mvp;

import com.example.vidhyashreeappaji.mvpdemo.model.ListItem;

public interface MVPView {

    void setItems(ListItem listItem);

    void showProgress();

    void hideProgress();

    void onFailure(Throwable t);

}

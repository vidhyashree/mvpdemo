package com.example.vidhyashreeappaji.mvpdemo.di.component;

import com.example.vidhyashreeappaji.mvpdemo.di.module.FragmentModule;
import com.example.vidhyashreeappaji.mvpdemo.di.scope.ActivityScope;
import com.example.vidhyashreeappaji.mvpdemo.view.ListViewFragment;

import dagger.Component;

//This Component is used to inject the Presenter in our ListViewFragment.
@ActivityScope
@Component(dependencies = AppComponent.class, modules = FragmentModule.class)
public interface FragmentComponent {
    void inject(ListViewFragment listViewFragment);

}

package com.example.vidhyashreeappaji.mvpdemo.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.vidhyashreeappaji.mvpdemo.R;
import com.example.vidhyashreeappaji.mvpdemo.view.ListViewFragment.MyListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MyListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Butterknife to bind view
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, new ListViewFragment()).commit();
        }
    }

    @Override
    public void setTitle(String s) {
        toolbar.setTitle(s);
    }
}

package com.example.vidhyashreeappaji.mvpdemo.retrofit;

import android.content.Context;

import java.io.File;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroFitAPIClient {
    private static final String BASE_URL = "https://dl.dropboxusercontent.com/";
    private static Retrofit retrofit = null;

    //instance of Retrofit object
    public static Retrofit getRetrofitInstance(Context context) {
        if (retrofit == null) {

            //Here a logging interceptor is created
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            //Http cache for offline cache
            File httpCacheDirectory = new File(context.getCacheDir(), "offlineCache");
            Cache cache = new Cache(httpCacheDirectory, 10 * 1024 * 1024);

            //okhttp interceptor
            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .cache(cache)
                    .addInterceptor(interceptor)
                    .build();

            //configuring with base_url
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}

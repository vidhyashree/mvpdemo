package com.example.vidhyashreeappaji.mvpdemo.model;

import java.util.ArrayList;

/*Model class for ListItems*/

public class ListItem {

    public int id;
    private String title;
    private ArrayList<RowItem> rows;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<RowItem> getRows() {
        return rows;
    }

    public void setRows(ArrayList<RowItem> rows) {
        this.rows = rows;
    }
}

package com.example.vidhyashreeappaji.mvpdemo;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;

import com.example.vidhyashreeappaji.mvpdemo.di.component.AppComponent;
import com.example.vidhyashreeappaji.mvpdemo.di.component.DaggerAppComponent;
import com.example.vidhyashreeappaji.mvpdemo.di.module.AppModule;
import com.example.vidhyashreeappaji.mvpdemo.di.module.ContextModule;
import com.example.vidhyashreeappaji.mvpdemo.utils.NetworkConnectivityReceiver;

public class MyApplication extends Application {

    private static MyApplication mInstance;
    private NetworkConnectivityReceiver myReceiver;
    private AppComponent component;

    public static MyApplication get(Context context) {
        return (MyApplication) context.getApplicationContext();
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        //initialize appComponent
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .contextModule(new ContextModule(this))
                .build();
        component.inject(this);
        registerReceiver();
    }

    public AppComponent component() {
        return component;
    }


    private NetworkConnectivityReceiver getMyReceiver() {
        if (myReceiver == null) {
            myReceiver = new NetworkConnectivityReceiver();
        }
        return myReceiver;
    }

    public void setConnectivityListener(NetworkConnectivityReceiver.NetworkConnectivityListener listener) {
        NetworkConnectivityReceiver.connectivityReceiverListener = listener;
    }

    /*Explicitly register receiver. Since declaring BraoadcastReceiver in manifest
     doesn't work in android OREO and above*/
    private void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        //noinspection deprecation
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        registerReceiver(getMyReceiver(), filter);
    }
}

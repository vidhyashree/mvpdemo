package com.example.vidhyashreeappaji.mvpdemo.mvp.presenter;

import com.example.vidhyashreeappaji.mvpdemo.model.ListItem;
import com.example.vidhyashreeappaji.mvpdemo.mvp.MVPView;
import com.example.vidhyashreeappaji.mvpdemo.mvp.interactor.Interactor;

//Presenter implementation
public class MainPresenterImpl implements MainPresenter, Interactor.OnFinishedListener {

    private MVPView mvpView;
    private Interactor interactor;

    //Constructor which takes view and model interfaces as parameters
    public MainPresenterImpl(MVPView mvpView, Interactor interactor) {
        this.mvpView = mvpView;
        this.interactor = interactor;
    }

    @Override
    public void onDestroy() {
        mvpView = null;
    }

    @Override
    public void onRefresh() {
        if (mvpView != null) {
            mvpView.showProgress();
        }
        interactor.fetchNewDataFromServer(this);
    }

    @Override
    public void onFinished(ListItem item) {
        if (mvpView != null) {
            mvpView.setItems(item);
            mvpView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mvpView != null) {
            mvpView.onFailure(t);
            mvpView.hideProgress();
        }
    }
    @Override
    public void fetchDatafromServer() {
        interactor.fetchNewDataFromServer(this);
    }
}

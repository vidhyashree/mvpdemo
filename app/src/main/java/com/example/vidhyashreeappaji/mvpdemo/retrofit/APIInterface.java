package com.example.vidhyashreeappaji.mvpdemo.retrofit;

import com.example.vidhyashreeappaji.mvpdemo.model.ListItem;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIInterface {
    @GET("s/2iodh4vg0eortkl/facts.json")
    Call<ListItem> getAllItems();
}

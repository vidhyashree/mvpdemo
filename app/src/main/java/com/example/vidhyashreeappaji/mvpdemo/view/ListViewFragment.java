package com.example.vidhyashreeappaji.mvpdemo.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.vidhyashreeappaji.mvpdemo.MyApplication;
import com.example.vidhyashreeappaji.mvpdemo.R;
import com.example.vidhyashreeappaji.mvpdemo.adapter.RowItemAdapter;
import com.example.vidhyashreeappaji.mvpdemo.di.component.DaggerFragmentComponent;
import com.example.vidhyashreeappaji.mvpdemo.di.module.FragmentModule;
import com.example.vidhyashreeappaji.mvpdemo.model.ListItem;
import com.example.vidhyashreeappaji.mvpdemo.mvp.MVPView;
import com.example.vidhyashreeappaji.mvpdemo.mvp.presenter.MainPresenter;
import com.example.vidhyashreeappaji.mvpdemo.utils.NetworkConnectivityReceiver;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
public class ListViewFragment extends Fragment implements MVPView, NetworkConnectivityReceiver.NetworkConnectivityListener {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeContainer;

    @Inject
    MainPresenter presenter;

    @Inject
    Context mContext;

    View rootView;
    MyListener mListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_listview, container, false);
        //Butterknife to bind the view
        ButterKnife.bind(this, rootView);
        //initialize swipe to refresh layout
        initRefreshLayout();
        //Injecting the presenter without intantiating it
        DaggerFragmentComponent.builder()
                .appComponent(MyApplication.get(getActivity()).component())
                .fragmentModule(new FragmentModule(this))
                .build()
                .inject(this);
        //call to fetch data from server
        presenter.fetchDatafromServer();

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (MyListener) context;

    }

    private void initRefreshLayout() {
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //code to refresh the list.
                swipeContainer.setRefreshing(false);
                presenter.onRefresh();
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    @Override
    public void setItems(ListItem item) {
        mListener.setTitle(item.getTitle());
        RowItemAdapter recyclerViewAdapter = new RowItemAdapter(mContext, item.getRows());
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerViewAdapter.getItemCount();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onFailure(Throwable t) {
        Toast.makeText(getActivity(), getString(R.string.error_message) + t.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        //set listener for network change
        MyApplication.get(getActivity()).setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        //handle on network change
        if (!isConnected) {
            Snackbar.make(getActivity().findViewById(R.id.snackbar_view), "Internet Unavailable!", Snackbar.LENGTH_INDEFINITE).
                    show();
        } else {
            Snackbar.make(getActivity().findViewById(R.id.snackbar_view), "Internet Available!", Snackbar.LENGTH_LONG).show();
            presenter.onRefresh();
        }
    }

    public interface MyListener {
        void setTitle(String title);
    }
}

package com.example.vidhyashreeappaji.mvpdemo.di.module;

import android.content.Context;

import com.example.vidhyashreeappaji.mvpdemo.mvp.MVPView;
import com.example.vidhyashreeappaji.mvpdemo.mvp.interactor.Interactor;
import com.example.vidhyashreeappaji.mvpdemo.mvp.interactor.InteractorImpl;
import com.example.vidhyashreeappaji.mvpdemo.mvp.presenter.MainPresenter;
import com.example.vidhyashreeappaji.mvpdemo.mvp.presenter.MainPresenterImpl;

import dagger.Module;
import dagger.Provides;

//This provides an instance of the MainPresenterImpl class.
//It takes the View’s interface  as it’s constructor argument.
@Module
public class FragmentModule {

    private MVPView mvpView;

    public FragmentModule(MVPView mvpView) {
        this.mvpView = mvpView;
    }

    @Provides
    public MVPView provideView() {
        return mvpView;
    }

    @Provides
    public MainPresenter providePresenter(MVPView view, Interactor interactor) {
        return new MainPresenterImpl(view, interactor);
    }

    @Provides
    public Interactor provideInteractor(Context context) {
        return new InteractorImpl(context);
    }
}
